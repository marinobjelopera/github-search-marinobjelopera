import axios from 'axios';

const token = '17eaed7f90d1ee001272b235b65d6ff39aa95e7a';

const ApiHandler = axios.create({
  baseURL: 'https://api.github.com/graphql',
  headers: {
    Authorization: `bearer ${token}`
  },
});
export default ApiHandler;
