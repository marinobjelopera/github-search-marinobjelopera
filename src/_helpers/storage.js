class Storage {
  constructor() {
    this.storage = window.localStorage;
    this.baseKey = 'gs';
  }

  /**
   * Inserts multiple items into browser local storage
   */
  setMultipleItems() {
    var args = arguments;

    for(let i = 0; i < args.length; i++) {
      let item = args[i];
      this.storage.setItem(`${this.baseKey}.${item.key}`, item.data);
    }
  }

  /**
   * Retrieves an item from browser local storage for given key
   * @param {String} key
   */
  getItem(key) {
    return this.storage.getItem(key);
  }
}
export default Storage;
