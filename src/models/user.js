class User {
  constructor(username, name, bio, email, avatar, url) {
    this.username = username
    this.name = name
    this.bio = bio
    this.email = email
    this.avatar = avatar
    this.url = url
  }
}
export default User;
