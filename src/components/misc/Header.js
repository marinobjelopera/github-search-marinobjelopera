import React from 'react';

class Header extends React.Component {
  state = {
    logoSrc: "./media/github-logo.png"
  };

  render() {
    return (
      <div className="gs-header">
        <div className="gs-header-wrapper">
          <div className="gs-header-logo">
            <img
              className="img-fluid"
              src={this.state.logoSrc}
              alt="GitHub" />
          </div>

          <div className="gs-header-title">
            <div className="gs-header-title-content">
              <div className="gs-title">
                GitHub
              <div className="gs-subtitle text-muted">
                  search
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Header;
