import React from 'react';

class ValueMissing extends React.Component {

  render() {
    return (
      <span className="text-invalid">
        {this.props.value}
      </span>
    );
  }

}
export default ValueMissing;
