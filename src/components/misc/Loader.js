import React from 'react';

class Loader extends React.Component {
  render() {
    return (
      <div id="gs-loader-outer">
        <div id="gs-loader-inner"></div>
      </div>
    );
  }
}
export default Loader;
