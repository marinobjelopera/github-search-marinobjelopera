import React from 'react';

class SearchBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  /**
   * Changes the component state on value change
   * @param {*} event
   */
  handleChange(event) {
    let value = event.target.value;

    this.setState({
      value
    });
  }

  /**
   * Performs a search on button click
   */
  handleClick() {
    this.props.performSearch(this.state.value);
  }

  render() {
    return (
      <div className="gs-search-wrapper">
        <div className="gs-search-input">
          <div className="gs-input-prepend">
            <button
              id="dropdown.toggle"
              className="gs-btn btn-outline dropdown-toggle"
              type="button"
              onClick={this.props.handleToggle}>
              by {this.props.searchType}
            </button>
            {this.props.dropdownIsToggled &&
              <div className="gs-dropdown-menu">
                <button
                  className="gs-dropdown-item"
                  value="repository"
                  onClick={this.props.onTypeChange}>
                  by repository
                </button>
                <button
                  className="gs-dropdown-item"
                  value="username"
                  onClick={this.props.onTypeChange}>
                  by username
                </button>
              </div>
            }
          </div>

          <input id="input.query"
            type="text"
            className="gs-input"
            placeholder={`search github by ${this.props.searchType}...`}
            onChange={this.handleChange} />
        </div>

        <div className="gs-btn submit primary" onClick={this.handleClick}>Search</div>
      </div>
    );
  }
}
export default SearchBar;
