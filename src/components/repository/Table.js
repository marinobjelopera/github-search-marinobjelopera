import React from 'react';
import TableRow from './TableRow';
import PaginationBar from '../pagination/PaginationBar';

class Table extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      itemsPerPage: 10,
      options: [
        10, 20, 30
      ],
    };
  }

  render() {
    let repositories = this.props.repositories.data;

    let options = this.state.options.map(option =>
      <option key={option} value={option}>{option}</option>
    );

    const items = repositories.map((repository) =>
      <TableRow
        key={repository.id}
        id={repository.id}
        repository={repository} />
    );

    return (
      <div className="gs-search-results">
        {this.props.repositories.totalCount < 1 &&
          <h2 className="text-center text-invalid">
            {this.props.title}
          </h2>
        }

        {this.props.repositories.totalCount > 0 &&
          <div>
            <h5 className="gs-title-2 text-center">
              {this.props.title}
            </h5>

            <div className="gs-ipp-filter">
              <div className="text-small" style={{ marginRight: '.5rem' }}>
                Items per page:
          </div>
              <select className="gs-select" onChange={this.props.onChange}>
                {options}
              </select>
            </div>

            <div className="gs-repository-list">
              {items}
            </div>

            <PaginationBar
              pageInfo={this.props.pageInfo}
              nextPageClick={this.props.nextPageClick}
              prevPageClick={this.props.prevPageClick} />
          </div>
        }
      </div>
    );
  }
}
export default Table;
