import React from 'react';

class TableRow extends React.Component {

  render() {
    const repository = this.props.repository;

    return (
      <div className="gs-repository-item">
        <div className="gs-title-3">
          {repository.name}
        </div>
        <div className="gs-repository-description gs-text text-muted">
          {repository.description}
        </div>
        <a href={repository.url}
          className="gs-link"
          target="_blank"
          rel="noopener noreferrer">
          GitHub Link
        </a>
      </div>
    );
  }

}
export default TableRow;
