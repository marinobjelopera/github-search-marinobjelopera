import React from 'react';
import Table from './repository/Table';
import ValueMissing from './misc/ValueMissing';

class UserDetails extends React.Component {

  valueExists(value) {
    return value !== null && value !== undefined && value !== '';
  }

  render() {
    const user = this.props.user

    return (
      <div className="gs-search-results">

        <div className="gs-user-wrapper">
          <div className="gs-user-avatar">
            <img className="img-fluid" src={user.avatar} alt={user.name} />
          </div>

          <div className="gs-user-description">
            <div className="gs-title-2">
              {this.valueExists(user.name)
                ? user.name
                : <ValueMissing value={"Name unavailable."} />
              }
            </div>
            <div className="gs-text text-muted">
              {this.valueExists(user.bio)
                ? user.bio
                : <ValueMissing value={"User did not provide a bio."} />
              }
            </div>
            <div className="gs-text">
              {this.valueExists(user.email)
                ? user.email
                : <ValueMissing value={"User did not set a public email address."} />
              }
            </div>
            {}
            <div className="gs-user-github">
              <a href={user.url} className="gs-link" target="_blank" rel="noopener noreferrer">
                Visit GitHub Profile
              </a>
            </div>
          </div>
        </div>

        {this.props.repositories.totalCount < 1 &&
          <div className="gs-title-3 text-center text-invalid" style={{ marginTop: '2rem' }}>
            User has no public repositories.
          </div>
        }

        {this.props.repositories.totalCount > 0 &&
          <Table
            title={`${user.name}'s Repositories`}
            repositories={this.props.repositories}
            pageInfo={this.props.pageInfo}
            nextPageClick={this.props.nextPageClick}
            prevPageClick={this.props.prevPageClick}
            onChange={this.props.onChange} />
        }
      </div>
    );
  }
}
export default UserDetails;
