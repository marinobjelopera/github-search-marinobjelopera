import React from 'react'
import ReactDOM from 'react-dom'
import { Header, SearchBar, Loader, UserDetails, Table } from './components';
import { UserService, RepositoryService } from './_services'
import { Repository, User } from './models'
import { Storage } from './_helpers';

const storage = new Storage();

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dropdownIsToggled: false,
      loaderIsVisible: false,
      searchType: 'repository',
      hasResults: false,
      user: null,
      repositories: {
        data: null,
        totalCount: 0,
      },
      repositoriesPerPage: 10,
      searchedQuery: null,
      pageInfo: null,
    };

    this.handleToggle = this.handleToggle.bind(this);
    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.nextPage = this.nextPage.bind(this);
    this.prevPage = this.prevPage.bind(this);
    this.onItemsPerPageChange = this.onItemsPerPageChange.bind(this);
    this.performSearch = this.performSearch.bind(this);
  }

  /**
   * On component mount, check if any data has been persisted
   * Load the most recent query
   */
  componentDidMount() {
    let type = storage.getItem('gs.type');
    let searchedQuery = storage.getItem('gs.query');
    let repositories = JSON.parse(storage.getItem('gs.repos'));
    let pageInfo = JSON.parse(storage.getItem('gs.pagination'));

    if (type !== undefined) {
      switch (type) {
        case 'username':
          this.setState({
            hasResults: true,
            searchType: type,
            user: JSON.parse(storage.getItem('gs.user')),
            repositories,
            pageInfo,
            searchedQuery,
          });
          break;
        case 'repository':
          this.setState({
            hasResults: true,
            searchType: type,
            repositories,
            pageInfo,
            searchedQuery,
          });
          break;

        default: break;
      }
    }
  }

  hideDropdownOnBodyClick(event) {
    let toggler = document.querySelector('#dropdown.toggle');
    let clickedInside = toggler.contains(event.target);

    if (this.state.dropdownIsToggled && !clickedInside) {
      this.setState({
        dropdownIsToggled: false
      });
    }
  }

  /**
   * Handles toggling of the dropdown menu
   */
  handleToggle() {
    this.setState({
      dropdownIsToggled: !this.state.dropdownIsToggled
    });
  }

  /**
   * Handles the click in the dropdown menu
   * Changes the type of the search request
   * @param {*} event
   */
  handleTypeChange(event) {
    let type = event.target.value;

    this.setState({
      dropdownIsToggled: !this.state.dropdownIsToggled,
      hasResults: false,
      searchType: type.toLowerCase(),
    });
  }

  performSearch(name) {
    if (name === undefined || name === '') {
      return;
    }

    this.setState({
      loaderIsVisible: true,
    });

    switch (this.state.searchType) {
      case 'repository':
        this.fetchRepositories(name);
        break;

      case 'username':
        this.fetchUser(name);
        break;

      default:
        alert('Unable to process request. Refresh the page and try again.');
        return;
    }
  }

  /**
   * Retrieves a GitHub user for specific name
   * @param {String} username
   */
  fetchUser(username) {
    UserService.fetchUsersByName(username)
      .then(({ data }) => {
        let count = data.data.search.userCount;

        if (count > 0) {
          let user = data.data.search.edges[0].node;
          let pageInfo = user.repositories.pageInfo;

          user.repositoryCount = user.repositories.totalCount;
          const repositories = {
            totalCount: user.repositories.totalCount,
            data: user.repositories.edges.map(repo =>
              new Repository(repo.node.id, repo.node.name, repo.node.description, repo.node.url)
            )
          };

          user = new User(
            user.login, user.name, user.bio, user.email, user.avatarUrl, user.url
          );

          this.setState({
            user,
            repositories,
            pageInfo,
            hasResults: true,
            loaderIsVisible: false,
            searchedQuery: username,
          });

          // Persist request
          storage.setMultipleItems(
            { key: 'type', data: 'username' },
            { key: 'query', data: username },
            { key: 'user', data: JSON.stringify(user) },
            { key: 'repos', data: JSON.stringify(repositories) },
            { key: 'pagination', data: JSON.stringify(pageInfo) }
          );
        }
        else {
          alert(`User with account name "${username}" has not been found.`);
          this.setState({
            loaderIsVisible: false,
          });
        }
      })
      .catch(error => console.log(error));
  }

  /**
   * Retrieves the repositories which include the given name in their repository name
   * @param {String} name
   */
  fetchRepositories(name) {
    RepositoryService.fetchRepositoriesByName(name)
      .then(({ data }) => {
        let response = data.data.search;

        let repositories = {
          data: response.edges.map(repo =>
            new Repository(repo.node.id, repo.node.name, repo.node.description, repo.node.url)
          ),
          totalCount: response.repositoryCount,
        };
        let pageInfo = response.pageInfo;

        this.setState({
          searchedQuery: name,
          repositories,
          pageInfo,
          hasResults: true,
          loaderIsVisible: false,
        });

        // Persist request
        storage.setMultipleItems(
          { key: 'type', data: 'repository' },
          { key: 'query', data: name },
          { key: 'repos', data: JSON.stringify(repositories) },
          { key: 'pagination', data: JSON.stringify(pageInfo) }
        );
      })
      .catch(error => console.log(error));
  }

  /**
   * Retrieves the previous page of the repository list
   */
  prevPage() {
    this.setState({
      loaderIsVisible: true,
    });

    let query = this.state.searchedQuery;
    let itemsPerPage = this.state.repositoriesPerPage;
    let cursor = this.state.pageInfo.startCursor;

    switch (this.state.searchType) {
      case 'repository':
        RepositoryService.fetchRepositoriesBeforeCursor(query, itemsPerPage, cursor)
          .then(({ data }) => {
            let response = data.data.search;

            let repositories = response.edges.map(repo =>
              new Repository(repo.node.id, repo.node.name, repo.node.description, repo.node.url)
            );

            this.setState({
              repositories: {
                data: repositories,
                totalCount: response.repositoryCount
              },
              pageInfo: response.pageInfo,
              loaderIsVisible: false,
            })
          })
          .catch(error => console.log(error));
        break;

      case 'username':
        UserService.fetchUserRepositoriesBeforeCursor(query, itemsPerPage, cursor)
          .then(({ data }) => {
            let response = data.data.user.repositories;

            let repositories = response.edges.map(repo =>
              new Repository(repo.node.id, repo.node.name, repo.node.description, repo.node.url)
            );

            let pageInfo = response.pageInfo;

            this.setState({
              repositories: {
                data: repositories,
                totalCount: response.totalCount
              },
              pageInfo,
              loaderIsVisible: false,
            });
          })
          .catch(error => console.log(error));
        break;

      default: break;
    }
  }

  /**
   * Retrieves the next page of the repository list
   */
  nextPage() {
    this.setState({
      loaderIsVisible: true
    });
    let query = this.state.searchedQuery;
    let itemsPerPage = this.state.repositoriesPerPage;
    let cursor = this.state.pageInfo.endCursor;

    switch (this.state.searchType) {
      case 'repository':
        RepositoryService.fetchRepositoriesAfterCursor(query, itemsPerPage, cursor)
          .then(({ data }) => {
            let response = data.data.search;
            let repositories = {
              data: response.edges.map(repo =>
                new Repository(repo.node.id, repo.node.name, repo.node.description, repo.node.url)
              ),
              totalCount: response.repositoryCount
            };
            let pageInfo = response.pageInfo;

            this.setState({
              repositories,
              pageInfo,
              loaderIsVisible: false,
            });
          })
          .catch(error => console.log(error));
        break;

      case 'username':
        UserService.fetchUserRepositoriesAfterCursor(query, itemsPerPage, cursor)
          .then(({ data }) => {
            let response = data.data.user.repositories;

            let repositories = response.edges.map(repo =>
              new Repository(repo.node.id, repo.node.name, repo.node.description, repo.node.url)
            );

            let pageInfo = response.pageInfo;

            this.setState({
              repositories: {
                data: repositories,
                totalCount: response.totalCount,
              },
              loaderIsVisible: false,
              pageInfo,
            });
          })
          .catch(error => console.log(error));
        break;

      default: break;
    }
  }

  /**
   * Retrieves the repository list with a specific number of repositories per page
   * @param {*} event
   */
  onItemsPerPageChange(event) {
    this.setState({
      loaderIsVisible: true,
    });

    let query = this.state.searchedQuery;
    let repositoriesPerPage = parseInt(event.target.value);

    switch (this.state.searchType) {
      case 'repository':
        RepositoryService.fetchRepositories(query, repositoriesPerPage)
          .then(({ data }) => {
            let response = data.data.search;

            let repositories = response.edges.map(repo =>
              new Repository(repo.node.id, repo.node.name, repo.node.description, repo.node.url)
            );

            this.setState({
              repositoriesPerPage,
              repositories: {
                data: repositories,
                totalCount: response.repositoryCount,
              },
              loaderIsVisible: false,
              pageInfo: response.pageInfo,
            });
          })
          .catch(error => console.log(error));
        break;
      case 'username':
        UserService.fetchUserRepositories(query, repositoriesPerPage)
          .then(({ data }) => {
            let response = data.data.user.repositories;

            let repositories = response.edges.map(repo =>
              new Repository(repo.node.id, repo.node.name, repo.node.description, repo.node.url)
            );

            let pageInfo = response.pageInfo;

            this.setState({
              repositoriesPerPage,
              repositories: {
                data: repositories,
                totalCount: response.totalCount
              },
              loaderIsVisible: false,
              pageInfo,
            });

          })
          .catch(error => console.log(error));
        break;
      default: break;
    }
  }

  render() {
    let isToggled = this.state.dropdownIsToggled;

    return (
      <div>
        {this.state.loaderIsVisible &&
          <Loader />
        }
        <Header />
        <SearchBar
          searchType={this.state.searchType}
          dropdownIsToggled={isToggled}
          handleToggle={this.handleToggle}
          onTypeChange={this.handleTypeChange}
          performSearch={this.performSearch} />

        <div className="gs__search__results">
          {this.state.hasResults && this.state.searchType === 'username' &&
            <UserDetails
              user={this.state.user}
              repositories={this.state.repositories}
              pageInfo={this.state.pageInfo}
              nextPageClick={this.nextPage}
              prevPageClick={this.prevPage}
              onChange={this.onItemsPerPageChange} />
          }
          {this.state.hasResults && this.state.searchType === 'repository' &&
            <Table
              title={
                this.state.repositories.totalCount > 0
                  ? `Repositories containing "${this.state.searchedQuery}"`
                  : `No results found for "${this.state.searchedQuery}"`
              }
              repositories={this.state.repositories}
              pageInfo={this.state.pageInfo}
              nextPageClick={this.nextPage}
              prevPageClick={this.prevPage}
              onChange={this.onItemsPerPageChange}
            />
          }
        </div>
      </div>
    );
  }
}

ReactDOM.render(
  <App />,
  document.querySelector('#github-search')
)
