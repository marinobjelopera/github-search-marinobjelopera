import { ApiHandler } from '../_helpers/index';

class RepositoryService {

  static fetchRepositoriesByName(name) {
    let query = `
      query {
        search(query: "${name} in:name", type:REPOSITORY, first:10) {
          repositoryCount
          pageInfo {
            hasNextPage
            hasPreviousPage
            startCursor
            endCursor
          }
          edges {
            node {
              ... on Repository {
                id
                name
                description
                url
                owner {
                  login
                }
              }
            }
          }
        }
      }
    `;

    return ApiHandler.post('', { query: query });
  }

  static fetchRepositories(name, itemsPerPage) {
    let query = `
      query {
        search(query: "${name}", type:REPOSITORY, first:${itemsPerPage}) {
          repositoryCount
          pageInfo {
            hasNextPage
            hasPreviousPage
            startCursor
            endCursor
          }
          edges {
            node {
              ... on Repository {
                id
                name
                description
                url
              }
            }
          }
        }
      }
    `;

    return ApiHandler.post('', { query });
  }

  static fetchRepositoriesAfterCursor(name, itemsPerPage, cursor) {
    let query = `
      query {
        search(query: "${name} in:name", type:REPOSITORY, first:${itemsPerPage}, after:"${cursor}") {
          repositoryCount
          pageInfo {
            hasNextPage
            hasPreviousPage
            startCursor
            endCursor
          }
          edges {
            node {
              ... on Repository {
                id
                name
                description
                url
              }
            }
          }
        }
      }
    `;

    return ApiHandler.post('', { query: query });
  }

  static fetchRepositoriesBeforeCursor(name, itemsPerPage, cursor) {
    let query = `
      query {
        search(query: "${name} in:name", type:REPOSITORY, last:${itemsPerPage}, before:"${cursor}") {
          repositoryCount
          pageInfo {
            hasNextPage
            hasPreviousPage
            startCursor
            endCursor
          }
          edges {
            node {
              ... on Repository {
                id
                name
                description
                url
              }
            }
          }
        }
      }
    `;

    return ApiHandler.post('', { query: query });
  }
}
export default RepositoryService;
