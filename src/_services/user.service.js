import { ApiHandler } from '../_helpers/index';

class UserService {

  /**
   * Retrieves user and their repositories
   * @param {String} name
   */
  static fetchUsersByName(name) {
    const QUERY = `
      query {
        search(query: "user:${name}", type: USER, first: 1) {
          edges {
            node {
              ... on User {
                avatarUrl
                name
                email
                bio
                url
                login
                repositories(first: 10) {
                  totalCount
                  pageInfo {
                    hasNextPage
                    hasPreviousPage
                    startCursor
                    endCursor
                  }
                  edges {
                    node {
                      id
                      name
                      description
                      url
                    }
                  }
                }
              }
            }
          }
          userCount
        }
      }
    `;

    return ApiHandler.post('', { query: QUERY });
  }

  /**
   * Retrieves user repositories after a given cursor
   * Used to retrieve next page of user repositories
   * @param {String} owner
   * @param {Int} count
   * @param {String} cursor
   */
  static fetchUserRepositoriesAfterCursor(owner, count, cursor) {
    let query = `
      {
        user(login: "${owner}") {
          repositories(first: ${count}, after:"${cursor}") {
            totalCount
            pageInfo {
              hasNextPage
              hasPreviousPage
              startCursor
              endCursor
            }
            edges {
              node {
                id
                name
                description
                url
              }
            }
          }
        }
      }
    `;

    return ApiHandler.post('', { query });
  }

  /**
   * Retrieves user repositories before a specific cursor
   * Used to retrieve previous page of user repositories
   * @param {String} owner
   * @param {Int} count
   * @param {String} cursor
   */
  static fetchUserRepositoriesBeforeCursor(owner, count, cursor) {
    let query = `
      {
        user(login: "${owner}") {
          repositories(last: ${count}, before:"${cursor}") {
            totalCount
            pageInfo {
              hasNextPage
              hasPreviousPage
              startCursor
              endCursor
            }
            edges {
              node {
                id
                name
                description
                url
              }
            }
          }
        }
      }
    `;

    return ApiHandler.post('', { query });
  }

  /**
   * Retrieves user repositories with a specific number of items
   * @param {String} owner
   * @param {Int} count
   */
  static fetchUserRepositories(owner, count) {
    let query = `
      {
        user(login: "${owner}") {
          repositories(first: ${count}) {
            totalCount
            pageInfo {
              hasNextPage
              hasPreviousPage
              startCursor
              endCursor
            }
            edges {
              node {
                id
                name
                description
                url
              }
            }
          }
        }
      }
    `;

    return ApiHandler.post('', { query });
  }
}

export default UserService;
