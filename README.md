# Github Search

GitHub search is a web application which allows you to search for repositories and/or users and their repositories
on GitHub by name.

# How to run it locally

To run the project, you need to have node.js and NPM installed
Run the following commands:

- npm install - to install the packages
- npm start - to run the project locally

# Architecture

The project architecture consists of React components utilizing the services to talk with GitHub's graphql API
and retrieves the given data based on the input query by the user. The user is able to search either for
repositories by their name, or GitHub user's by their account name.
Services are utilizing the Axios package for simpler network layer communication.
Data persistance is achieved by saving the results to browser's local storage and on application load, the last
search request is automatically loaded.

# Third-party libraries

- Axios [https://github.com/axios/axios]
- Bootstrap 4 (A few components) [https://getbootstrap.com/]
